FROM php:8.1-fpm

ENV APP_HOME /var/www/html
ARG HOST_UID=1000
ARG HOST_GID=1000
ENV USERNAME=www-data

LABEL version="1.0"
LABEL description="API - CTJ"

RUN apt update \
    && apt install -y zlib1g-dev g++ libicu-dev zip libzip-dev zip libmagickwand-dev libpq-dev libxml2-dev \
    && apt-get clean all \
    && docker-php-ext-install intl opcache \
    && pecl install apcu \
    && docker-php-ext-enable apcu \
    && docker-php-ext-install gd \
    && docker-php-ext-enable gd \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && docker-php-ext-install pdo pdo_pgsql \
    && docker-php-ext-install soap \
    && rm -rf /tmp/* \
    && rm -rf /var/list/apt/* \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# ver que hacemos con composer. Si bien están acostumbrados a usarlo globalmente, cada proyecto debe tener su composer.phar y se debe respetar y utilizar ese.
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://get.symfony.com/cli/installer | bash -s -- --install-dir=/usr/local/bin

RUN mkdir -p $APP_HOME/public && \
    mkdir -p /home/$USERNAME && chown $USERNAME:$USERNAME /home/$USERNAME \
    && usermod -o -u $HOST_UID $USERNAME -d /home/$USERNAME \
    && groupmod -o -g $HOST_GID $USERNAME \
    && chown -R ${USERNAME}:${USERNAME} $APP_HOME

COPY ./docker/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY ./docker/php.ini-production /usr/local/etc/php/php.ini

# set working directory
WORKDIR $APP_HOME

USER ${USERNAME}

# copy source files
COPY --chown=${USERNAME}:${USERNAME} . $APP_HOME/
COPY --chown=${USERNAME}:${USERNAME} .env $APP_HOME/
COPY --chown=${USERNAME}:${USERNAME} .env.local $APP_HOME/

RUN composer install --optimize-autoloader --no-interaction --no-progress --no-dev

USER root