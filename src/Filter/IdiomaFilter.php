<?php

namespace App\Filter;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class IdiomaFilter extends SQLFilter
{

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {

        if ($targetEntity->hasField('idioma')) {
            $codigoIdioma = $this->getParameter('codigo_idioma');

            return $targetTableAlias . '.idioma = ' . $codigoIdioma;
        }

        return '';
    }
}
