<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use Doctrine\ORM\EntityManagerInterface;

class IdiomaSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private EntityManagerInterface $em,
        private string $idioma)
    {
        $this->em = $em;
        $this->idioma = $idioma;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $codigoIdioma = $request->query->get('codigoIdioma', $this->idioma);
        $this->em->getFilters()->enable('idioma_filter')->setParameter('codigo_idioma', $codigoIdioma);
        if (strpos($request->getPathInfo(), '/gestion') === 0) {
            $this->em->getFilters()->disable('idioma_filter');
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }
}
