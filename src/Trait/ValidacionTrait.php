<?php

namespace App\Trait;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait ValidacionTrait {
    
    public function getGrupo($clase, $data, $idioma, $version = null) {

        $clase = get_class($clase);
        if (!$data->grupo) {
            $claseGrupo = $this->em->getRepository($clase)->findOneBy(array(), ["grupo" => "DESC"]);
            $grupo = (!$claseGrupo) ? 1 : $claseGrupo->getGrupo() + 1;
        } else {
            $grupo = $data->grupo;
            if (!$version) {
                $result = $this->em->getRepository($clase)->findOneBy(["idioma" => $data->idioma, "grupo" => $grupo]);
                if ($result) {
                    throw new BadRequestHttpException("Ya existe en idioma {$idioma->getDescripcion()}");
                }
            }
        }

        return $grupo;
    }
}