<?php

namespace App\Pagination;

class PaginatedCollection
{
    /** @var array */
    private $items;

    /** @var int */
    private $total;

    /** @var int */
    private $count;

    /** @var int */
    private $totalPages;

    public function __construct(array $items, $totalItems, $totalPages)
    {
        $this->items = $items;
        $this->total = $totalItems;
        $this->totalPages = $totalPages;
        $this->count = count($items);
    }

    public function getItems() {
        return $this->items;
    }

    public function getTotal() {
        return $this->total;
    }

    public function getTotalPages() {
        return $this->totalPages;
    }

    public function getCount() {
        return $this->count;
    }
}
