<?php

namespace App\Pagination;

use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;

class PaginationFactory
{
    public function createCollection(QueryBuilder $qb, $page, $resultadosPorPagina = 10)
    {
        $adapter = new QueryAdapter($qb, true, false);
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta->setMaxPerPage($resultadosPorPagina);
        $pagerfanta->setCurrentPage($page);

        $items = [];
        foreach ($pagerfanta->getCurrentPageResults() as $result) {
            $items[] = $result;
        }

        $paginatedCollection = new PaginatedCollection($items, $pagerfanta->getNbResults(), $pagerfanta->getNbPages());

        return $paginatedCollection;
    }
}
