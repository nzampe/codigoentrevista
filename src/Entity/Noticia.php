<?php

namespace App\Entity;

use App\Repository\NoticiaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NoticiaRepository::class)]
class Noticia
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $titulo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subtitulo = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $cuerpo = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $fecha = null;

    #[ORM\Column(nullable: true)]
    private array $imagenes = [];

    #[ORM\Column]
    private ?bool $publicada = null;

    #[ORM\Column]
    private ?int $grupo = null;

    #[ORM\Column(length: 2)]
    private ?string $idioma = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $previsualizacion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): static
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getSubtitulo(): ?string
    {
        return $this->subtitulo;
    }

    public function setSubtitulo(?string $subtitulo): static
    {
        $this->subtitulo = $subtitulo;

        return $this;
    }

    public function getCuerpo(): ?string
    {
        return $this->cuerpo;
    }

    public function setCuerpo(string $cuerpo): static
    {
        $this->cuerpo = $cuerpo;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): static
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImagenes(): array
    {
        return $this->imagenes;
    }

    public function setImagenes(array $imagenes): static
    {
        $this->imagenes = $imagenes;

        return $this;
    }

    public function isPublicada(): ?bool
    {
        return $this->publicada;
    }

    public function setPublicada(bool $publicada): static
    {
        $this->publicada = $publicada;

        return $this;
    }

    public function getGrupo(): ?int
    {
        return $this->grupo;
    }

    public function setGrupo(int $grupo): static
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getIdioma(): ?string
    {
        return $this->idioma;
    }

    public function setIdioma(string $idioma): static
    {
        $this->idioma = $idioma;

        return $this;
    }

    public function getPrevisualizacion(): ?string
    {
        return $this->previsualizacion;
    }

    public function setPrevisualizacion(?string $previsualizacion): static
    {
        $this->previsualizacion = $previsualizacion;

        return $this;
    }
}
