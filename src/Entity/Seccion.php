<?php

namespace App\Entity;

use App\Repository\SeccionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: SeccionRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Seccion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nombre = null;

    #[ORM\Column(nullable: true)]
    private ?int $orden = null;

    #[ORM\Column(nullable: true)]
    private ?array $banner = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $descTitulo = null;

    #[ORM\Column]
    private ?int $grupo = null;

    #[ORM\Column(length: 2)]
    private ?string $idioma = null;

    #[ORM\ManyToOne]
    private ?Plantilla $plantilla = null;

    #[ORM\Column(nullable: true)]
    private ?array $contenido = null;

    #[ORM\Column(nullable: true)]
    private ?bool $publicada = null;

    #[ORM\OneToMany(mappedBy: 'padreSeccion', targetEntity: Seccion::class)]
    #[ORM\OrderBy(["orden" => "ASC"])]
    private Collection $subsecciones;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $fechaUltimaActualizacion = null;

    #[Ignore]
    #[ORM\ManyToOne(targetEntity: self::class)]
    private ?self $padreSeccion = null;

    public function __construct()
    {
        $this->subsecciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): static
    {
        $this->orden = $orden;

        return $this;
    }

    public function getBanner(): ?array
    {
        return $this->banner;
    }

    public function setBanner(?array $banner): static
    {
        $this->banner = $banner;

        return $this;
    }

    public function getDescTitulo(): ?string
    {
        return $this->descTitulo;
    }

    public function setDescTitulo(?string $descTitulo): static
    {
        $this->descTitulo = $descTitulo;

        return $this;
    }

    public function getGrupo(): ?int
    {
        return $this->grupo;
    }

    public function setGrupo(int $grupo): static
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getIdioma(): ?string
    {
        return $this->idioma;
    }

    public function setIdioma(string $idioma): static
    {
        $this->idioma = $idioma;

        return $this;
    }

    public function getPlantilla(): ?Plantilla
    {
        return $this->plantilla;
    }

    public function setPlantilla(?Plantilla $plantilla): static
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    public function getContenido(): ?array
    {
        return $this->contenido;
    }

    public function setContenido(?array $contenido): static
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function isPublicada(): ?bool
    {
        return $this->publicada;
    }

    public function setPublicada(?bool $publicada): static
    {
        $this->publicada = $publicada;

        return $this;
    }

    /**
     * @return Collection<int, Subseccion>
     */
    public function getSubsecciones(): Collection
    {
        return $this->subsecciones;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getFechaUltimaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaUltimaActualizacion;
    }

    public function setFechaUltimaActualizacion(?\DateTimeInterface $fechaUltimaActualizacion): static
    {
        $this->fechaUltimaActualizacion = $fechaUltimaActualizacion;

        return $this;
    }

    public function getPadreSeccion(): ?self
    {
        return $this->padreSeccion;
    }

    public function setPadreSeccion(?self $padreSeccion): static
    {
        $this->padreSeccion = $padreSeccion;

        return $this;
    }

    /**
     #[ORM\PostLoad]
     * Set the value of codigoPlantilla
     *
     * @return  self
     */ 
    #[ORM\PostLoad]
    public function getCodigoPlantilla()
    {
        return $this->plantilla->getCodigo();

        return $this;
    }
}
