<?php

namespace App\Entity;

use App\Repository\UsuarioRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: UsuarioRepository::class)]
class Usuario implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(['default'])]
    private ?string $username = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(['default'])]
    #[Ignore]
    private ?string $password = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Ignore]
    private ?\DateTimeInterface $fDesde = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Ignore]
    private ?\DateTimeInterface $fHasta = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Ignore]
    private ?\DateTimeInterface $fCarga = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFDesde(): ?\DateTimeInterface
    {
        return $this->fDesde;
    }

    public function setFDesde(\DateTimeInterface $fDesde): static
    {
        $this->fDesde = $fDesde;

        return $this;
    }

    public function getFHasta(): ?\DateTimeInterface
    {
        return $this->fHasta;
    }

    public function setFHasta(?\DateTimeInterface $fHasta): static
    {
        $this->fHasta = $fHasta;

        return $this;
    }

    public function getFCarga(): ?\DateTimeInterface
    {
        return $this->fCarga;
    }

    public function setFCarga(\DateTimeInterface $fCarga): static
    {
        $this->fCarga = $fCarga;

        return $this;
    }
}
