<?php

namespace App\Entity;

use App\Repository\ConfiguracionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfiguracionRepository::class)]
class Configuracion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?ConfiguracionTipo $tipo = null;

    #[ORM\Column]
    private array $valor = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?ConfiguracionTipo
    {
        return $this->tipo;
    }

    public function setTipo(?ConfiguracionTipo $tipo): static
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getValor(): array
    {
        return $this->valor;
    }

    public function setValor(array $valor): static
    {
        $this->valor = $valor;

        return $this;
    }
}
