<?php
namespace App\DTO\Request\Usuario;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class RegistroRequest
{
    #[Groups(['default'])]
    #[Assert\NotBlank]
    #[Assert\Length(max: 20)]
    public ?string $username = null;

    #[Groups(['default'])]
    #[Assert\NotBlank]
    #[Assert\Length(min: 8)]
    public ?string $password = null;

}
