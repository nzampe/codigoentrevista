<?php
namespace App\DTO\Request\Home;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


class CrearHomeRequest
{

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'version' es obligatorio")]
    public ?int $version = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'contenido' es obligatorio")]
    public ?array $contenido = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'idioma' es obligatorio")]
    public ?string $idioma;

}
