<?php
namespace App\DTO\Request\Home;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Idioma;

class EditarHomeRequest
{   
    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'informacion' es obligatorio")]
    #[Assert\Collection(
            fields: [
                'logo' => 	new Assert\Type('string'),
                'videoPrincipal'=> new Assert\Type('string'),
                'email'=> new Assert\Type('string'),
                'telefono'=> new Assert\Type('string'),
                'facebook'=> new Assert\Type('string'),
                'instagram'=> new Assert\Type('string'),
                'linkedln'=> new Assert\Type('string'),
                'direccion'=> new Assert\Type('string') 
            ]
    )]
    public ?array $informacion = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'sectores' es obligatorio")]
    #[Assert\Collection(
            fields: [
                'productos' => new Assert\Optional([
                    new Assert\Type('array'),
                    new Assert\Count(['min' => 1, 'max' => 4]),
                    new Assert\All([
                        new Assert\Collection(
                            fields:[
                                'image' => new Assert\Type('string'),
                                'titulo' => new Assert\Type('string'),
                                'contenido' => new Assert\Type('string')
                            ]
                        )])
            ]),
                'logistica' => new Assert\Collection(
                    fields:[
                        'banner' => new Assert\Type('string'),
                        'titulo' => new Assert\Type('string'),
                        'contenido' => new Assert\Type('string'),
                        'virtudes' => new Assert\Optional([
                            new Assert\Type('array'),
                            new Assert\Count(['min' => 2, 'max' => 2]),
                        ]),
                    ]
                ),
                'ubicacion' => new Assert\Collection(
                    fields:[
                        'video' => new Assert\Type('string'),
                        'latitud' => new Assert\Type('string'),
                        'longitud' => new Assert\Type('string'),
                    ]
                ),

            ]
    )]
    public ?array $sectores = null;
}
