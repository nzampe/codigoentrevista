<?php
namespace App\DTO\Request\Seccion;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Idioma;

class EditarSeccionRequest
{
    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Nombre' es obligatorio")]
    public ?string $nombre = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Banner' es obligatorio")]
    public ?array $banner = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(allowNull: true)]
    public ?string $descTitulo = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Plantilla' es obligatorio")]
    public ?string $plantilla = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Contenido' es obligatorio")]
    public ?array $contenido = null;

}
