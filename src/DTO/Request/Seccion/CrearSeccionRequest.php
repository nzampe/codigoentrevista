<?php
namespace App\DTO\Request\Seccion;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class CrearSeccionRequest
{
    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Nombre' es obligatorio")]
    public ?string $nombre = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Plantilla' es obligatorio")]
    public ?string $plantilla = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Contenido' es obligatorio")]
    public ?array $contenido = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Idioma' es obligatorio")]
    public ?string $idioma;

    #[Groups(['default'])]
    #[Assert\NotBlank(allowNull: true)]
    public ?int $grupo = null;
}
