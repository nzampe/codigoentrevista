<?php
namespace App\DTO\Request\Noticia;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Idioma;

class EditarNoticiaRequest
{
    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Titulo' es obligatorio")]
    public ?string $titulo = null;

    #[Groups(['default'])]
    public ?string $subtitulo = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Previsualizacion' es obligatorio")]
    public ?string $previsualizacion = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Cuerpo' es obligatorio")]
    public ?string $cuerpo;

    #[Groups(['default'])]
    public ?array $imagenes = null;

}
