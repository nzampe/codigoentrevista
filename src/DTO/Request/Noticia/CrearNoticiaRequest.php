<?php
namespace App\DTO\Request\Noticia;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Idioma;

class CrearNoticiaRequest
{
    #[Groups(['default'])]
    #[Assert\NotBlank(message: "'Idioma' es obligatorio")]
    public ?string $idioma = null;

    #[Groups(['default'])]
    #[Assert\NotBlank(allowNull: true)]
    public ?int $grupo = null;

}
