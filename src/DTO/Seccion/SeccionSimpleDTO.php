<?php
namespace App\DTO\Seccion;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use App\Entity\Subseccion;

class SeccionSimpleDTO
{
    public ?int $id = null;
    public ?string $nombre = null;
    public ?int $orden = null;
    public ?string $codPlantilla = null;
    public ?array $subsecciones = [];

    
    public function __construct($id, $nombre, $codPlantilla, $orden)
    {
        $this->id           = $id;
        $this->nombre       = $nombre;
        $this->orden        = $orden;
        $this->codPlantilla = $codPlantilla;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function getCodPlantilla(): ?string
    {
        return $this->codPlantilla;
    }

    public function getSubsecciones(): ?string
    {
        return $this->codPlantilla;
    }
}
