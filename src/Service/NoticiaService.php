<?php

namespace App\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;

use App\Entity\Noticia;
use App\Entity\Idioma;

use App\Trait\ValidacionTrait;

use App\DTO\Request\Noticia\CrearNoticiaRequest;
use App\DTO\Request\Noticia\EditarNoticiaRequest;

use App\Repository\NoticiaRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NoticiaService
{
    use ValidacionTrait;

    public function __construct(
        protected EntityManagerInterface $em,
        protected Connection $connection,
        protected NoticiaRepository $noticiaRepository
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->noticiaRepository = $noticiaRepository;
    }

    public function crear(
        $data
    ): Noticia {

        try {
            $this->connection->beginTransaction();
            $idioma = $this->em->getRepository(Idioma::class)->findOneByCodigo($data->idioma);
            if (!$idioma) {
                throw new BadRequestHttpException("El idioma no existe");
            }

            $grupo = $this->getGrupo(new Noticia(), $data, $idioma);

            $noticia = (new Noticia())
                ->setIdioma($data->idioma)
                ->setGrupo($grupo)
                ->setCuerpo("<h3>Subtitulo</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                ")
                ->setPublicada(false);

            $this->noticiaRepository->add($noticia, true);
            $this->connection->commit();

            return $noticia;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function editar(
        EditarNoticiaRequest $data,
        Noticia $noticia = null
    ): Noticia {

        try {
            $this->connection->beginTransaction();
            if (!$noticia) {
                throw new BadRequestHttpException("La noticia no existe");
            }
            if ($noticia->isPublicada()) {
                throw new BadRequestHttpException("La noticia no se puede editar ya que se encuentra publicada");
            }
            $imagenes = !empty($data->imagenes) ? $data->imagenes : [];

            $noticia
                ->setTitulo($data->titulo)
                ->setSubtitulo($data->subtitulo)
                ->setPrevisualizacion($data->previsualizacion)
                ->setCuerpo($data->cuerpo)
                ->setImagenes($imagenes);

            $this->em->flush();
            $this->connection->commit();

            return $noticia;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function obtenerNoticias(
        array $data
    ) {
        $noticias = $this->em->getRepository(Noticia::class)->filtrar($data);

        return $noticias;
    }

    public function publicarGrupo(
        int $grupo
    ) {
        $this->connection->beginTransaction();
        try {
            $noticia = $this->em->getRepository(Noticia::class)->findOneByGrupo($grupo);
            if (!$noticia) {
                throw new NotFoundHttpException("El grupo no existe");
            }

            // Verifico que las noticias no estén publicadas
            $noticias = $this->em->getRepository(Noticia::class)->findBy(["grupo" => $grupo, "publicada" => false]);
            if (!$noticias) {
                throw new NotFoundHttpException("Las noticias ya están publicadas");
            }
            $idiomas = $this->em->getRepository(Idioma::class)->findAll();
            foreach ($idiomas as $valueI) {
                $noticiaIdioma = $this->em->getRepository(Noticia::class)->findBy(["grupo" => $grupo, "idioma" => $valueI->getCodigo()]);
                if (!$noticiaIdioma) {
                    throw new NotFoundHttpException("No se pueden publicar las noticias, ya que falta la de idioma {$valueI->getDescripcion()}");
                }
            }
            foreach ($noticias as $key => $noticia) {
                $noticia->setFecha(new \DateTime());
                $noticia->setPublicada(true);
            }

            $this->em->flush();
            $this->connection->commit();
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function publicar(
        Noticia $noticia = null
    ) {
        $this->connection->beginTransaction();
        try {
            if (!$noticia) {
                throw new NotFoundHttpException("La noticia no existe");
            }
            if ($noticia->isPublicada()) {
                throw new BadRequestHttpException("La noticia ya se encuentra publicada");
            }
            $noticias = $this->em->getRepository(Noticia::class)->findBy(["grupo" => $noticia->getGrupo(), "publicada" => true]);
            if (!$noticias) {
                $idiomas = $this->em->getRepository(Idioma::class)->findAll();
                if (count($idiomas) > 1) {
                    $noticias = $this->em->getRepository(Noticia::class)->findBy(["grupo" => $noticia->getGrupo(), "publicada" => false]);
                    if ($noticias) {
                        throw new NotFoundHttpException("La noticia no se puede publicar individualmente");
                    }
                }
            }
            $noticia->setFecha(new \DateTime());
            $noticia->setPublicada(true);

            // Parche para que tailwind interprete los saltos de línea de acuerdo al editor de html
            $cuerpo = str_replace("<p></p>", "<br>", $noticia->getCuerpo());
            
            $noticia->setCuerpo($cuerpo);

            $this->em->flush();
            $this->connection->commit();
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function obtenerPorGrupo($grupo) {
        $noticias = $this->em->getRepository(Noticia::class)->findByGrupo($grupo, ["id" => "DESC"]);

        return $noticias;
    }

    public function obtenerGrupos() {
        $gruposNoticias = $this->em->getRepository(Noticia::class)->getGrupos();
        $noticias = [];
        foreach ($gruposNoticias as $key => $value) {
            $noticia = $this->em->getRepository(Noticia::class)->findOneBy(["grupo" => $value["grupo"], "idioma" => "es"], ["id" => "ASC"]);
            if (!$noticia) {
                $noticia = $this->em->getRepository(Noticia::class)->findOneByGrupo($value["grupo"], ["id" => "ASC"]);
            }
            $grupos[$key]["id"] = $noticia->getId();
            $grupos[$key]["grupo"] = $noticia->getGrupo();
            $grupos[$key]["titulo"] = $noticia->getTitulo();
            $grupos[$key]["imagenes"] = $noticia->getImagenes();
            $grupos[$key]["fecha"] = $noticia->getFecha();
            $grupos[$key]["publicada"] = $noticia->isPublicada();
            $grupos[$key]["idioma"] = $noticia->getIdioma();
        }

        return $grupos;
    }
}
