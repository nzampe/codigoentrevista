<?php

namespace App\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;

use App\Entity\Home;
use App\Entity\Idioma;

use App\DTO\Request\Home\CrearHomeRequest;

use App\Repository\HomeRepository;

use App\Trait\ValidacionTrait;

class HomeService
{
    use ValidacionTrait;

    public function __construct(
        protected EntityManagerInterface $em,
        protected Connection $connection,
        protected HomeRepository $homeRepository
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->homeRepository = $homeRepository;
    }

    public function guardar(
        CrearHomeRequest $data
    ): Home {

        try {
            $this->connection->beginTransaction();
            $idioma = $this->em->getRepository(Idioma::class)->findOneByCodigo($data->idioma);
            if (!$idioma) {
                throw new BadRequestHttpException("El idioma no existe");
            }

            // Ver con múltiples idiomas
            $home = $this->em->getRepository(Home::class)->findOneBy(["version" => $data->version]);
            if (!$home) {
                throw new NotFoundHttpException("La versión de la home no existe");
            }

            if ($home->isPublicada()) {
                $nuevaVersionHome = (new Home())
                ->setContenido($data->contenido)
                ->setVersion($home->getVersion() + 1)
                ->setIdioma($data->idioma)
                ->setPublicada(false);

                $this->homeRepository->add($nuevaVersionHome, false);
                $response = $nuevaVersionHome;
            }
            else {
                $home->setContenido($data->contenido);
                $response = $home;
            }

            $this->em->flush();
            $this->connection->commit();

            return $response;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function obtener(
        array $data = []
    ) {
        $home = $this->em->getRepository(Home::class)->filtrar($data);
        if (!empty($data["seccion"])) {
            $sector = $data["seccion"];
            if ($sector != "sectores" && $sector != "informacion") {
                throw new BadRequestHttpException("El valor del parámetro 'seccion' debe ser 'informacion' o 'sectores'");
            }
            return $home[0]->getContenido()[$sector];
        }

        return $home;
    }

    public function publicar(
        int $version
    ): Home {

        try {
            $this->connection->beginTransaction();

            // Ver con múltiples idiomas
            $homeAPublicar = $this->em->getRepository(Home::class)->findOneBy(["version" => $version]);
            if (!$homeAPublicar) {
                throw new NotFoundHttpException("No existe esta versión de la home");
            }

            if ($homeAPublicar->isPublicada()) {
                throw new NotFoundHttpException("Esta versión de la home ya se encuentra publicada");
            }

            $homePublicada = $this->em->getRepository(Home::class)->findOneBy(["publicada" => true]);
            $homePublicada->setPublicada(false);

            $homeAPublicar->setPublicada(true);

            $this->em->flush();
            $this->connection->commit();

            return $homeAPublicar;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }
}
