<?php
namespace App\Service;

use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\WebDAV\WebDAVAdapter;
use Sabre\DAV\Client;

class WEBDAVService
{
    protected $filesystem;

    public function __construct(string $basUrl)
    {
        $client = new Client([
            'baseUri'  => $basUrl
        ]);

        $adapter = $this->getAdapter($client);
        $this->filesystem = new Filesystem($adapter);
    }

    protected function getAdapter($client) : FilesystemAdapter
    {
        return new WebDAVAdapter($client);
    }

    public function upload($file, $ubicacionRelativa, $name) {
        $location = $ubicacionRelativa.$name;
        $content = file_get_contents($file->getFileInfo()->getPathname());
        // $content = preg_replace("/^pack('H*','EFBBBF')/", '', file_get_contents($file->getFileInfo()->getPathname()));
        $this->filesystem->write($location, $content);
    }
}
