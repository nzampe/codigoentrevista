<?php

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;

use App\Entity\Configuracion;
use App\Entity\ConfiguracionTipo;
use App\Entity\ConfiguracionSubtipo;

use App\Trait\ValidacionTrait;

use App\Repository\ConfiguracionRepository;
use Sabre\DAV\Exception\BadRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ConfiguracionService
{
    use ValidacionTrait;

    public function __construct(
        protected EntityManagerInterface $em,
        protected Connection $connection,
        protected ConfiguracionRepository $configuracionRepository
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->configuracionRepository = $configuracionRepository;
    }

    public function guardar(
        array $data
    ): Configuracion {

        try {
            $this->connection->beginTransaction();
            $configuracionTipo = $this->em->getRepository(ConfiguracionTipo::class)->findOneByCodigo(strtoupper(trim($data["tipo"])));
            if (!$configuracionTipo) {
                throw new BadRequestHttpException("El tipo de configuración no existe");
            }

            $configuracion = $this->em->getRepository(Configuracion::class)->findOneByTipo($configuracionTipo);
            if(!$configuracion) {
                $configuracion = new Configuracion();
                $this->configuracionRepository->add($configuracion, false);
            }

            $configuracion
                ->setTipo($configuracionTipo)
                ->setValor($data["valor"]);

            $this->em->flush();
            $this->connection->commit();

            return $configuracion;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function obtener() 
    {
        $configuraciones = $this->em->getRepository(Configuracion::class)->findAll();

        return $configuraciones;
    }

    public function obtenerTipos() 
    {
        $configuracionTipos = $this->em->getRepository(ConfiguracionTipo::class)->findAll();

        return $configuracionTipos;
    }
}
