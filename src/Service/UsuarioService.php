<?php
namespace App\Service;

use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UsuarioService
{
    public function __construct(
        protected UserPasswordHasherInterface  $passwordHasher,
        protected UsuarioRepository  $usuarioRepository
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->usuarioRepository = $usuarioRepository;
    }

    public function create(
        string $username,
        string $password = null,
        ?array $roles = []
    ): Usuario {
        $now = new \DateTime();
        
        $usuario = (new Usuario())
            ->setFCarga($now)
            ->setFDesde($now)
            ->setUsername($username)
            ->setRoles($roles)
        ;

        $newHashedPassword = $this->passwordHasher->hashPassword(
            $usuario,
            $password
        );

        $usuario->setPassword($newHashedPassword);

        $this->usuarioRepository->add($usuario, true);
        
        return $usuario;
    }

    public function isPasswordValid(PasswordAuthenticatedUserInterface $usuario, string $plaintextPassword)
    {
        return $this->passwordHasher->isPasswordValid($usuario, $plaintextPassword);
    }
}
