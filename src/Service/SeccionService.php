<?php

namespace App\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use Symfony\Component\String\Slugger\AsciiSlugger;

use App\Entity\Seccion;
use App\Entity\Idioma;
use App\Entity\Plantilla;

use App\DTO\Request\Seccion\CrearSeccionRequest;
use App\DTO\Request\Seccion\EditarSeccionRequest;

use App\Trait\ValidacionTrait;

use App\Repository\SeccionRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SeccionService
{
    use ValidacionTrait;

    public function __construct(
        protected EntityManagerInterface $em,
        protected Connection $connection,
        protected SeccionRepository $seccionRepository
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->seccionRepository = $seccionRepository;
    }

    public function crear(
        CrearSeccionRequest $data,
    ): Seccion {

        try {
            $this->connection->beginTransaction();
            $idioma = $this->em->getRepository(Idioma::class)->findOneByCodigo($data->idioma);
            if (!$idioma) {
                throw new BadRequestHttpException("El idioma no existe");
            }
            $plantilla = $this->em->getRepository(Plantilla::class)->findOneByCodigo(strtoupper($data->plantilla));
            if (!$plantilla) {
                throw new BadRequestHttpException("La plantilla no existe");
            }

            $grupo = $this->getGrupo(new Seccion(), $data, $idioma);

            $ultSeccion = $this->em->getRepository(Seccion::class)->findOneBy(['padreSeccion' => null], ["orden" => "DESC"]);
            $orden = !empty($ultSeccion) ? $ultSeccion->getOrden() + 1 : 1;

            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($data->nombre)->lower();

            $seccion = (new Seccion())
                ->setNombre($data->nombre)
                ->setPlantilla($plantilla)
                ->setContenido($data->contenido)
                ->setOrden($orden)
                ->setIdioma($data->idioma)
                ->setGrupo($grupo)
                ->setPublicada(false)
                ->setFechaUltimaActualizacion(new \DateTime)
                ->setSlug($slug);

            $this->seccionRepository->add($seccion, true);
            $this->connection->commit();

            return $seccion;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function guardar(
        EditarSeccionRequest $data,
        Seccion $seccion = null
    ): Seccion {

        try {
            $this->connection->beginTransaction();
            if (!$seccion) {
                throw new NotFoundHttpException("La sección no existe");
            }
            if ($seccion->isPublicada()) {
                throw new BadRequestHttpException("La sección no se puede editar ya que se encuentra publicada");
            }
            $plantilla = $this->em->getRepository(Plantilla::class)->findOneByCodigo(strtoupper($data->plantilla));
            if (!$plantilla) {
                throw new BadRequestHttpException("La plantilla no existe");
            }

            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($data->nombre)->lower();

            $seccion
                ->setNombre($data->nombre)
                ->setBanner($data->banner)
                ->setDescTitulo($data->descTitulo)
                ->setPlantilla($plantilla)
                ->setContenido($data->contenido)
                ->setFechaUltimaActualizacion(new \DateTime)
                ->setSlug($slug);

            $this->em->flush();
            $this->connection->commit();

            return $seccion;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }

    public function obtenerSeccion(
        array $data,
        Seccion $seccion = null
    ) {
        $seccion = $this->em->getRepository(Seccion::class)->getSeccion($data, $seccion);

        if (!$seccion) {
            throw new BadRequestHttpException("La sección no existe");
        }

        return $seccion;
    }

    public function obtenerSeccionesPublicadas()
    {
        $secciones = $this->em->getRepository(Seccion::class)->findByPublicada(true);

        return $secciones;
    }

    public function obtenerPorGrupo($grupo)
    {
        $secciones = $this->em->getRepository(Seccion::class)->findByGrupo($grupo, ["id" => "DESC"]);

        return $secciones;
    }

    public function obtenerGrupos()
    {
        $gruposSecciones = $this->em->getRepository(Seccion::class)->getGrupos();
        $seccions = [];
        foreach ($gruposSecciones as $key => $value) {
            $seccion = $this->em->getRepository(Seccion::class)->findOneBy(["grupo" => $value["grupo"], "publicada" => false, 'padreSeccion' => null, "idioma" => "es"], ["orden" => "ASC"]);
            if (!$seccion) {
                $seccion = $this->em->getRepository(Seccion::class)->findOneBy(["grupo" => $value["grupo"], "publicada" => false, 'padreSeccion' => null], ["orden" => "ASC"]);
            }
            if ($seccion) {
                $grupos[] = $seccion;
            }
        }

        return $grupos;
    }

    public function publicar(Seccion $seccionAPublicar = null)
    {
        try {
            $this->connection->beginTransaction();
            if (!$seccionAPublicar) {
                throw new NotFoundHttpException("La sección no existe");
            }

            if ($seccionAPublicar->isPublicada()) {
                throw new BadRequestHttpException("La sección ya está publicada");
            }

            $seccionPOld = $this->em->getRepository(Seccion::class)->findOneBy(
                [
                    "grupo" => $seccionAPublicar->getGrupo(),
                    "idioma" => $seccionAPublicar->getIdioma(),
                    "publicada" => true
                ]
            );

            $seccionBorrador = clone $seccionAPublicar;
            $seccionBorrador->setFechaUltimaActualizacion(null);
            $this->em->persist($seccionBorrador);

            if (!$seccionAPublicar->getPadreSeccion()) {
                $seccionesHijasP = $seccionPOld ? $seccionPOld->getSubsecciones() : [];
                $seccionesHijasB = $seccionAPublicar->getSubsecciones();
                // Reordeno los padres de las secciones
                foreach ($seccionesHijasP as $seccionP) {
                    $seccionP->setPadreSeccion($seccionAPublicar);
                }
                foreach ($seccionesHijasB as $seccionH) {
                    $seccionH->setPadreSeccion($seccionBorrador);
                }
            } else {
                $padreSeccionSAPublicar = $this->em->getRepository(Seccion::class)->find($seccionAPublicar->getPadreSeccion());
                $padreSeccionPSAPublicar = $this->em->getRepository(Seccion::class)->findOneBy(
                    [
                        "grupo" => $padreSeccionSAPublicar->getGrupo(),
                        "idioma" => $padreSeccionSAPublicar->getIdioma(),
                        "publicada" => true
                    ]
                );
                if (!$padreSeccionPSAPublicar) {
                    throw new BadRequestHttpException("La sección padre de la sección a publicar no está publicada");
                }
                $seccionAPublicar->setPadreSeccion($padreSeccionPSAPublicar);
            }

            $seccionAPublicar
                ->setPublicada(true)
                ->setFechaUltimaActualizacion(new \DateTime);

            if ($seccionPOld) {
                $this->em->remove($seccionPOld);
            }

            $this->em->flush();
            $this->connection->commit();

            return $seccionBorrador;
        } catch (\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }
    }
}
