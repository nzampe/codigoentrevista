<?php

namespace App\Repository;

use App\Entity\Noticia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Pagination\PaginationFactory;

/**
 * @extends ServiceEntityRepository<Noticia>
 *
 * @method Noticia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Noticia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Noticia[]    findAll()
 * @method Noticia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoticiaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginationFactory $paginationFactory)
    {
        $this->paginador = $paginationFactory;
        parent::__construct($registry, Noticia::class);
    }

    public function filtrar($params)
    {
        \extract($params);

        $qb = $this->createQueryBuilder('n');
        
        if (!empty($grupo)) {
            $qb->andWhere('n.grupo = :grupo')
            ->setParameter('grupo', $grupo)
            ->orderBy("n.grupo", "DESC");
        }

        if ($publicada) {
            $qb->andWhere('n.publicada = true')
            ->orderBy("n.fecha", "DESC");
        }
        
        if (isset($pagina)) {
            return $this->paginador->createCollection($qb, $pagina, $cantidad);
        } else {
            return $qb->getQuery()->getResult();
        }
    }

    public function getGrupos()
    {
        return $this->createQueryBuilder('n')
        ->select('n.grupo')
        ->distinct()
        ->orderBy("n.grupo", "DESC")
        ->getQuery()->getResult();
    }

    public function add(Noticia $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Noticia $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return Noticia[] Returns an array of Noticia objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('n.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Noticia
//    {
//        return $this->createQueryBuilder('n')
//            ->andWhere('n.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
