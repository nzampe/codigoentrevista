<?php

namespace App\Repository;

use App\Entity\ConfiguracionTipo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConfiguracionTipo>
 *
 * @method ConfiguracionTipo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfiguracionTipo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfiguracionTipo[]    findAll()
 * @method ConfiguracionTipo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfiguracionTipoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfiguracionTipo::class);
    }

//    /**
//     * @return ConfiguracionTipo[] Returns an array of ConfiguracionTipo objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ConfiguracionTipo
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
