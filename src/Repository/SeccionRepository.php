<?php

namespace App\Repository;

use App\Entity\Seccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Pagination\PaginationFactory;

/**
 * @extends ServiceEntityRepository<Seccion>
 *
 * @method Seccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seccion[]    findAll()
 * @method Seccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginationFactory $paginationFactory)
    {
        $this->paginador = $paginationFactory;
        parent::__construct($registry, Seccion::class);
    }

    public function filtrar($params)
    {
        \extract($params);

        $qb = $this->createQueryBuilder('s')
        ->leftJoin('App:Plantilla', 'p', 'WITH', 's.plantilla = p');
        
        if (!empty($grupo)) {
            $qb->andWhere('s.grupo = :grupo')
            ->setParameter('grupo', $grupo)
            ->orderBy("s.grupo", "DESC");
        }

        if ($publicada) {
            $qb->andWhere('s.publicada = true');
        }
        
        $qb->orderBy("s.orden", "ASC");

        if (isset($pagina)) {
            return $this->paginador->createCollection($qb, $pagina, $cantidad);
        } else {
            return $qb->getQuery()->getResult();
        }
    }

    public function getSeccion($data, $seccion = null)
    {
        \extract($data);

        $qb = $this->createQueryBuilder('s')
        ->select('s.id as seccionId, s.nombre, s.descTitulo, s.banner, s.contenido, s.idioma, p.codigo as codigoPlantilla, s.publicada, s.fechaUltimaActualizacion, s.slug')
        ->leftJoin('App:Plantilla', 'p', 'WITH', 's.plantilla = p');

        if (!empty($seccion)) {
            $qb->andWhere("s.id = :seccion")
            ->setParameter('seccion', $seccion);
        }

        $qb->andWhere("s.publicada = :publicada")
        ->setParameter('publicada', $publicada);
    
        if(!empty($slug)) {
            $qb->andWhere("s.slug = :slug")
            ->setParameter('slug', $slug);
        }

        return $qb->getQuery()->getResult();
    }

    public function add(Seccion $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getGrupos()
    {
        return $this->createQueryBuilder('s')
        ->select('s.grupo, s.orden')
        ->distinct()
        ->where('s.publicada = false')
        ->orderBy("s.orden", "ASC")
        ->addOrderBy("s.grupo", "ASC")
        ->getQuery()->getResult();
    }

//    /**
//     * @return Seccion[] Returns an array of Seccion objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Seccion
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
