<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Connection;

use App\DTO\Request\Usuario\RegistroRequest;
use App\Repository\UsuarioRepository;
use App\Service\UsuarioService;


#[Route('/usuario', name: 'app_usuario')]
class UsuarioController extends AbstractController
{
    public function __construct(
        private Connection $connection,
        private UsuarioService $usuarioService,
        private UsuarioRepository $usuarioRepository
    ) {
        $this->usuarioService    = $usuarioService;
        $this->usuarioRepository = $usuarioRepository;
        $this->connection        = $connection;
    }

    
    #[Route('/registro', name: 'registro', methods: ['POST'])]
    public function registro(
        #[MapRequestPayload] RegistroRequest $payload
    ): Response 
    {
        $username = trim($payload->username);
        $password = trim($payload->password);
        $usuario = $this->usuarioRepository->findOneByUsername($username);

        if($usuario) {
            throw new BadRequestHttpException("El usuario {$username} ya se encuentra registrado");
        }

        try {
            $this->connection->beginTransaction();
            $usuario = $this->usuarioService->create($username, $password, ["ROLE_ADMIN"]);

            $this->connection->commit();
        } catch(\Throwable $th) {
            $this->connection->rollback();

            throw $th;
        }

        return $this->json(compact('usuario'));

    }

    
    #[Route('', name: 'obtener_usuario', methods: ['GET'])]
    public function obtenerUsuario(
    ): Response 
    {
        $usuario = $this->getUser();

        return $this->json(compact('usuario'));

    }
}
