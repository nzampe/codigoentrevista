<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Connection;

use App\Entity\Noticia;

use App\Service\NoticiaService;
use FOS\RestBundle\Request\ParamFetcherInterface;

#[Route('/noticia', name: 'noticia')]
class NoticiaController extends AbstractController
{
    public function __construct(
        private Connection $connection,
        private NoticiaService $noticiaService
    ) {
        $this->noticiaService    = $noticiaService;
        $this->connection        = $connection;
    }

    #[Route('', name: 'obtener_noticias_publicadas', methods: ['GET'])]
    #[Rest\QueryParam(name: 'pagina', description: 'Pagina', strict: false, nullable: true, default: 1)]
    #[Rest\QueryParam(name: 'cantidad', description: 'Cantidad', strict: false, nullable: true, default: 10)]
    public function obtenerPublicadas(ParamFetcherInterface $paramFetcher): Response 
    {
        $params = $paramFetcher->all();
        $data = $params;
        $data["publicada"] = true;

        $noticias = $this->noticiaService->obtenerNoticias($data);

        return $this->json($noticias);
    }

    #[Route('/{noticia}', name: 'obtener_noticia', methods: ['GET'])]
    public function obtener(
        Noticia $noticia = null
    ): Response 
    {
        if(!$noticia || !$noticia->isPublicada()) {
            throw new BadRequestHttpException("La noticia no existe");
        }

        return $this->json(compact('noticia'));
    }
}
