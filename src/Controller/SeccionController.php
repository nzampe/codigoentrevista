<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Connection;

use App\Entity\Seccion;

use App\Service\SeccionService;


#[Route('/seccion', name: 'seccion')]
class SeccionController extends AbstractFOSRestController
{
    public function __construct(
        private Connection $connection,
        private SeccionService $seccionService
    ) {
        $this->connection        = $connection;
        $this->seccionService    = $seccionService;
    }

    #[Route('', name: 'obtener_lista_secciones', methods: ['GET'])]
    public function obtenerLista(): Response 
    {
        $secciones = $this->seccionService->obtenerSeccionesPublicadas();

        return $this->json($secciones);
    }

    #[Route('/slug/{slug}', name: 'obtener_seccion_slug_publicada', methods: ['GET'])]
    public function obtenerPorSlug(
        string $slug
    ): Response 
    {
        $data["slug"] = $slug;
        $data["publicada"] = true;
        $seccion = $this->seccionService->obtenerSeccion($data);

        return $this->json(compact('seccion'));
    }

    #[Route('/slug/{slug}/preview', name: 'obtener_seccion_slug_preview', methods: ['GET'])]
    public function obtenerPorSlugPreview(
        string $slug
    ): Response 
    {
        $data["slug"] = $slug;
        $data["publicada"] = false;
        $seccion = $this->seccionService->obtenerSeccion($data);

        return $this->json(compact('seccion'));
    }

    #[Route('/{seccion}', name: 'obtener_seccion', methods: ['GET'])]
    public function obtener(
        Seccion $seccion = null
    ): Response 
    {
        $data["publicada"] = true;
        $seccion = $this->seccionService->obtenerSeccion($data, $seccion);

        return $this->json(compact('seccion'));
    }

    
}
