<?php

namespace App\Controller\Gestion;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Connection;

use App\Entity\Seccion;

use App\Service\SeccionService;
use App\Service\WEBDAVService;


#[Route('/gestion/imagen', name: 'imagen')]
class ImagenGestionController extends AbstractFOSRestController
{
    protected $webdavService;

    public function __construct(WEBDAVService $webdavService)
    {
        $this->webdavService = $webdavService;
    }

    #[Route('', name: 'guadar_imagen', methods: ['POST'])]
    #[Rest\FileParam(name: 'imagen', description: 'imagen', strict: true, nullable: false)]
    public function guardar(
        WEBDAVService $webdavService,
        ParamFetcherInterface $paramFetcher
    ): Response 
    {
        $imagen = $paramFetcher->get('imagen');
        $nombreImagen = md5(strtotime("now")) . "." . $imagen->getClientOriginalExtension();

        $targetDirectory = "public/";
        $this->webdavService->upload($imagen, $targetDirectory, $nombreImagen);
        
        return $this->json(compact('nombreImagen'));
    }
}
