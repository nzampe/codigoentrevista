<?php

namespace App\Controller\Gestion;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;

use App\Entity\Configuracion;
use App\Entity\ConfiguracionTipo;

use App\Service\ConfiguracionService;


#[Route('/gestion/configuracion', name: 'configuracion')]
class ConfiguracionGestionController extends AbstractFOSRestController
{
    protected $configuracionService;

    public function __construct(ConfiguracionService $configuracionService)
    {
        $this->configuracionService = $configuracionService;
    }

    #[Route('', name: 'guadar_configuracion', methods: ['POST'])]
    #[Rest\RequestParam(name: 'tipo', description: 'Tipo', strict: true, nullable: false, allowBlank: false)]
    #[Rest\RequestParam(name: 'valor', description: 'Valor', strict: true, nullable: false, allowBlank: false)]
    public function guardar(
        ParamFetcherInterface $paramFetcher
    ): Response
    {
        $params = $paramFetcher->all();
        $configuracion = $this->configuracionService->guardar($params);
        
        return $this->json(compact('configuracion'));
    }

    #[Route('', name: 'obtener_configuraciones_gestion', methods: ['GET'])]
    public function obtenerBorrador(
        ParamFetcherInterface $paramFetcher
    ): Response 
    {
        $configuraciones = $this->configuracionService->obtener();
        
        return $this->json(compact('configuraciones'));
    }

    #[Route('/tipo', name: 'obtener_tipos_configuracion_gestion', methods: ['GET'])]
    public function obtenerTipos(): Response 
    {
        $configuracionesTipos = $this->configuracionService->obtenerTipos();
        
        return $this->json(compact('configuracionesTipos'));
    }
}