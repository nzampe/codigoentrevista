<?php

namespace App\Controller\Gestion;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\DBAL\Connection;

use App\Entity\Seccion;

use App\DTO\Request\Seccion\CrearSeccionRequest;
use App\DTO\Request\Seccion\EditarSeccionRequest;
use App\DTO\Request\Home\EditarHomeRequest;

use App\Service\SeccionService;


#[Route('/gestion/seccion', name: 'gestion_seccion')]
class SeccionGestionController extends AbstractFOSRestController
{
    public function __construct(
        private Connection $connection,
        private SeccionService $seccionService
    ) {
        $this->connection        = $connection;
        $this->seccionService    = $seccionService;
    }

    #[Route('', name: 'crear_seccion', methods: ['POST'])]
    public function crear(
        #[MapRequestPayload] CrearSeccionRequest $payload
    ): Response 
    {
        $payload->nombre     = trim($payload->nombre);
        $payload->plantilla  = trim($payload->plantilla);

        $seccion = $this->seccionService->crear($payload);

        return $this->json(compact('seccion'));
    }

    #[Route('/{seccion}/guardar', name: 'guardar_seccion', methods: ['POST'])]
    public function guardar(
        #[MapRequestPayload] EditarSeccionRequest $payload,
        Seccion $seccion = null
    ): Response 
    {
        $payload->nombre     = trim($payload->nombre);
        $payload->descTitulo = trim($payload->descTitulo);
        $payload->plantilla  = trim($payload->plantilla);

        $seccion = $this->seccionService->guardar($payload, $seccion);

        return $this->json(compact('seccion'));
    }

    #[Route('/{seccion}/publicar', name: 'publicar_seccion', methods: ['POST'])]
    public function publicar(Seccion $seccion = null) : Response 
    {
        $seccion = $this->seccionService->publicar($seccion);

        return $this->json(compact('seccion'));
    }

    #[Route('/grupo', name: 'obtener_lista_grupo', methods: ['GET'])]
    public function obtenerListaGrupos(): Response 
    {
        $grupos = $this->seccionService->obtenerGrupos();

        return $this->json($grupos);
    }

    #[Route('/slug/{slug}', name: 'obtener_seccion_slug_gestion', methods: ['GET'])]
    #[Rest\QueryParam(name: 'slug', description: 'Slug', strict: true, nullable: false)]
    public function obtenerPorSlug(
        string $slug
    ): Response 
    {
        $data["publicada"] = false;
        $data["slug"] = $slug;
        $seccion = $this->seccionService->obtenerSeccion($data);

        return $this->json(compact('seccion'));
    }

    #[Route('/{seccion}', name: 'obtener_seccion_gestion', methods: ['GET'])]
    public function obtener(
        Seccion $seccion = null
    ): Response 
    {
        $seccion = $this->seccionService->obtenerSeccion(["publicada" => false], $seccion);

        return $this->json(compact('seccion'));
    }

    #[Route('/grupo/{grupo}', name: 'obtener_lista_por_grupo', methods: ['GET'])]
    public function obtenerSeccionesPorGrupo(int $grupo): Response 
    {
        $grupos = $this->seccionService->obtenerPorGrupo($grupo);

        return $this->json($grupos);
    }
}
