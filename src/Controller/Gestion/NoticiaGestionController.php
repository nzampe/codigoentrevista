<?php

namespace App\Controller\Gestion;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\DBAL\Connection;

use App\Entity\Noticia;

use App\DTO\Request\Noticia\CrearNoticiaRequest;
use App\DTO\Request\Noticia\EditarNoticiaRequest;

use App\Service\NoticiaService;


#[Route('/gestion/noticia', name: 'gestion_noticia')]
class NoticiaGestionController extends AbstractFOSRestController
{
    public function __construct(
        private Connection $connection,
        private NoticiaService $noticiaService
    ) {
        $this->connection        = $connection;
        $this->noticiaService    = $noticiaService;
    }

    #[Route('', name: 'obtener_lista_noticias', methods: ['GET'])]
    #[Rest\QueryParam(name: 'grupo', description: 'Grupo', strict: true, nullable: false)]
    #[Rest\QueryParam(name: 'pagina', description: 'Pagina', strict: false, nullable: true, default: 1)]
    #[Rest\QueryParam(name: 'cantidad', description: 'Cantidad', strict: false, nullable: true, default: 10)]
    public function obtenerLista(ParamFetcherInterface $paramFetcher): Response 
    {
        $params = $paramFetcher->all();
        $data = $params;
        $data["publicada"] = false;

        $noticias = $this->noticiaService->obtenerNoticias($data);

        return $this->json($noticias);
    }

    #[Route('', name: 'crear_noticia', methods: ['POST'])]
    public function crear(
        #[MapRequestPayload] CrearNoticiaRequest $payload
    ): Response
    {
        $payload->idioma = $payload->idioma;
        $payload->grupo  = $payload->grupo;

        $noticia = $this->noticiaService->crear($payload);

        return $this->json(compact('noticia'));
    }

    #[Route('/{noticia}/editar', name: 'editar_noticia', methods: ['POST'])]
    public function editar(
        #[MapRequestPayload] EditarNoticiaRequest $payload,
        Noticia $noticia = null
    ): Response 
    {
        $payload->titulo           = trim($payload->titulo);
        $payload->subtitulo        = trim($payload->subtitulo);
        $payload->previsualizacion = trim($payload->previsualizacion);
        $payload->cuerpo           = trim($payload->cuerpo);
        $payload->imagenes         = $payload->imagenes;

        $noticia = $this->noticiaService->editar($payload, $noticia);

        return $this->json(compact('noticia'));
    }

    #[Route('/grupo', name: 'obtener_lista_grupos', methods: ['GET'])]
    public function obtenerListaGrupos(): Response 
    {
        $grupos = $this->noticiaService->obtenerGrupos();

        return $this->json($grupos);
    }

    #[Route('/{noticia}', name: 'obtener_noticia', methods: ['GET'])]
    public function obtener(
        Noticia $noticia = null
    ): Response
    {
        if(!$noticia) {
            throw new NotFoundHttpException("La noticia no existe");
        }

        return $this->json(compact('noticia'));
    }

    #[Route('/grupo/{grupo}', name: 'obtener_lista_por_grupo', methods: ['GET'])]
    public function obtenerNoticiasPorGrupo(int $grupo): Response 
    {
        $grupos = $this->noticiaService->obtenerPorGrupo($grupo);

        return $this->json($grupos);
    }

    #[Route('/publicar', name: 'publicar_grupo_noticias', methods: ['POST'])]
    #[Rest\RequestParam(name: 'grupo', description: 'Grupo', strict: true, nullable: false)]
    public function publicarGrupo(ParamFetcherInterface $paramFetcher): Response 
    {
        $params = $paramFetcher->all();
        \extract($params);

        $this->noticiaService->publicarGrupo($grupo);

        return $this->json(["mensaje" => "Las noticias han sido publicadas"]);
    }

    #[Route('/{noticia}/publicar', name: 'publicar_noticia', methods: ['POST'])]
    public function publicar(Noticia $noticia = null): Response 
    {
        $this->noticiaService->publicar($noticia);
            
        return $this->json(["mensaje" => "La noticia ha sido publicada"]);
    }
}
