<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Service\ConfiguracionService;


#[Route('/configuracion', name: 'home')]
class ConfiguracionController extends AbstractFOSRestController
{
    protected $configuracionService;

    public function __construct(ConfiguracionService $configuracionService)
    {
        $this->configuracionService = $configuracionService;
    }

    #[Route('', name: 'obtener_configuraciones', methods: ['GET'])]
    public function obtenerPublicada(
        ParamFetcherInterface $paramFetcher
    ): Response 
    {
        $configuraciones = $this->configuracionService->obtener();
        
        return $this->json(compact('configuraciones'));
    }
}