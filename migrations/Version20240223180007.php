<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240223180007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE seccion ADD padre_seccion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE seccion ADD fecha_ultima_actualizacion TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE seccion ADD CONSTRAINT FK_E0BD15C945A6B40C FOREIGN KEY (padre_seccion_id) REFERENCES seccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E0BD15C945A6B40C ON seccion (padre_seccion_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE seccion DROP CONSTRAINT FK_E0BD15C945A6B40C');
        $this->addSql('DROP INDEX IDX_E0BD15C945A6B40C');
        $this->addSql('ALTER TABLE seccion DROP padre_seccion_id');
        $this->addSql('ALTER TABLE seccion DROP fecha_ultima_actualizacion');
    }
}
