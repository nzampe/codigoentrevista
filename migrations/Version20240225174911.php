<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240225174911 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE configuracion ADD tipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion ADD subtipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion DROP tipo');
        $this->addSql('ALTER TABLE configuracion DROP subtipo');
        $this->addSql('ALTER TABLE configuracion ADD CONSTRAINT FK_682CCAF1A9276E6C FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE configuracion ADD CONSTRAINT FK_682CCAF1E245C6A3 FOREIGN KEY (subtipo_id) REFERENCES configuracion_subtipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_682CCAF1A9276E6C ON configuracion (tipo_id)');
        $this->addSql('CREATE INDEX IDX_682CCAF1E245C6A3 ON configuracion (subtipo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE configuracion DROP CONSTRAINT FK_682CCAF1A9276E6C');
        $this->addSql('ALTER TABLE configuracion DROP CONSTRAINT FK_682CCAF1E245C6A3');
        $this->addSql('DROP INDEX IDX_682CCAF1A9276E6C');
        $this->addSql('DROP INDEX IDX_682CCAF1E245C6A3');
        $this->addSql('ALTER TABLE configuracion ADD tipo VARCHAR(75) NOT NULL');
        $this->addSql('ALTER TABLE configuracion ADD subtipo VARCHAR(75) NOT NULL');
        $this->addSql('ALTER TABLE configuracion DROP tipo_id');
        $this->addSql('ALTER TABLE configuracion DROP subtipo_id');
    }
}
