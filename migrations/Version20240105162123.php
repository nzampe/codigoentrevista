<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240105162123 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE estadistica ADD home BOOLEAN');
        $this->addSql('ALTER TABLE logistica ADD publicada BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE logistica ADD grupo INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE estadistica DROP home');
        $this->addSql('ALTER TABLE logistica DROP publicada');
        $this->addSql('ALTER TABLE logistica DROP grupo');
    }
}
