<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240105145607 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE caracteristica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE estadistica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE logistica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE caracteristica (id INT NOT NULL, titulo VARCHAR(255) NOT NULL, cuerpo TEXT NOT NULL, imagenes JSON DEFAULT NULL, orden INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE estadistica (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, valor VARCHAR(255) NOT NULL, grupo INT NOT NULL, publicada BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE logistica (id INT NOT NULL, caracteristicas_id INT DEFAULT NULL, titulo VARCHAR(255) NOT NULL, desc_titulo VARCHAR(255) DEFAULT NULL, subtitulo VARCHAR(255) DEFAULT NULL, cuerpo TEXT NOT NULL, imagenes JSON DEFAULT NULL, idioma VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5D33CC21B9087426 ON logistica (caracteristicas_id)');
        $this->addSql('ALTER TABLE logistica ADD CONSTRAINT FK_5D33CC21B9087426 FOREIGN KEY (caracteristicas_id) REFERENCES caracteristica (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE caracteristica_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE estadistica_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE logistica_id_seq CASCADE');
        $this->addSql('ALTER TABLE logistica DROP CONSTRAINT FK_5D33CC21B9087426');
        $this->addSql('DROP TABLE caracteristica');
        $this->addSql('DROP TABLE estadistica');
        $this->addSql('DROP TABLE logistica');
    }
}
