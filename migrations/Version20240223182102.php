<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240223182102 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE subtipo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE subtipo (id INT NOT NULL, tipo_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A9986FCCA9276E6C ON subtipo (tipo_id)');
        $this->addSql('ALTER TABLE subtipo ADD CONSTRAINT FK_A9986FCCA9276E6C FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE subtipo_id_seq CASCADE');
        $this->addSql('ALTER TABLE subtipo DROP CONSTRAINT FK_A9986FCCA9276E6C');
        $this->addSql('DROP TABLE subtipo');
    }
}
