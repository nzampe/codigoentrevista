<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240118135719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE plantilla_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE plantilla (id INT NOT NULL, codigo VARCHAR(75) NOT NULL, descripcion VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE seccion ADD plantilla_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE seccion ADD contenido JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE seccion DROP tipo_seccion');
        $this->addSql('ALTER TABLE seccion ADD CONSTRAINT FK_E0BD15C9A08F3969 FOREIGN KEY (plantilla_id) REFERENCES plantilla (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E0BD15C9A08F3969 ON seccion (plantilla_id)');
        $this->addSql('ALTER TABLE subseccion ADD plantilla_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subseccion ADD contenido JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE subseccion ADD CONSTRAINT FK_ABDFBD06A08F3969 FOREIGN KEY (plantilla_id) REFERENCES plantilla (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_ABDFBD06A08F3969 ON subseccion (plantilla_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE seccion DROP CONSTRAINT FK_E0BD15C9A08F3969');
        $this->addSql('ALTER TABLE subseccion DROP CONSTRAINT FK_ABDFBD06A08F3969');
        $this->addSql('DROP SEQUENCE plantilla_id_seq CASCADE');
        $this->addSql('DROP TABLE plantilla');
        $this->addSql('DROP INDEX IDX_ABDFBD06A08F3969');
        $this->addSql('ALTER TABLE subseccion DROP plantilla_id');
        $this->addSql('ALTER TABLE subseccion DROP contenido');
        $this->addSql('DROP INDEX IDX_E0BD15C9A08F3969');
        $this->addSql('ALTER TABLE seccion ADD tipo_seccion VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE seccion DROP plantilla_id');
        $this->addSql('ALTER TABLE seccion DROP contenido');
    }
}
