<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240226152237 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE contacto_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE informacion_subseccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE producto_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE subseccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE caracteristica_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE estadistica_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE logistica_id_seq CASCADE');
        $this->addSql('ALTER TABLE informacion_subseccion DROP CONSTRAINT fk_77b4435a25cba699');
        $this->addSql('ALTER TABLE logistica DROP CONSTRAINT fk_5d33cc21b9087426');
        $this->addSql('ALTER TABLE subseccion DROP CONSTRAINT fk_abdfbd067a5a413a');
        $this->addSql('ALTER TABLE subseccion DROP CONSTRAINT fk_abdfbd06a08f3969');
        $this->addSql('DROP TABLE informacion_subseccion');
        $this->addSql('DROP TABLE logistica');
        $this->addSql('DROP TABLE producto');
        $this->addSql('DROP TABLE contacto');
        $this->addSql('DROP TABLE subseccion');
        $this->addSql('DROP TABLE caracteristica');
        $this->addSql('DROP TABLE estadistica');
        $this->addSql('ALTER TABLE configuracion DROP CONSTRAINT fk_682ccaf1a9276e6c');
        $this->addSql('DROP INDEX idx_682ccaf1a9276e6c');
        $this->addSql('ALTER TABLE configuracion DROP tipo_id');
        $this->addSql('ALTER TABLE configuracion DROP fecha_publicada');
        $this->addSql('ALTER TABLE configuracion DROP publicada');
        $this->addSql('ALTER TABLE configuracion DROP idioma');
        $this->addSql('ALTER TABLE configuracion RENAME COLUMN valor TO tipo');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE contacto_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE informacion_subseccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE producto_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE subseccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE caracteristica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE estadistica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE logistica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE informacion_subseccion (id INT NOT NULL, subseccion_id INT NOT NULL, contenido TEXT NOT NULL, imagenes JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_77b4435a25cba699 ON informacion_subseccion (subseccion_id)');
        $this->addSql('CREATE TABLE logistica (id INT NOT NULL, caracteristicas_id INT DEFAULT NULL, titulo VARCHAR(255) NOT NULL, desc_titulo VARCHAR(255) DEFAULT NULL, subtitulo VARCHAR(255) DEFAULT NULL, cuerpo TEXT NOT NULL, imagenes JSON DEFAULT NULL, idioma VARCHAR(2) NOT NULL, publicada BOOLEAN NOT NULL, grupo INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_5d33cc21b9087426 ON logistica (caracteristicas_id)');
        $this->addSql('CREATE TABLE producto (id INT NOT NULL, titulo VARCHAR(255) NOT NULL, desc_titulo VARCHAR(255) NOT NULL, subtitulo VARCHAR(255) DEFAULT NULL, cuerpo TEXT NOT NULL, grupo INT NOT NULL, publicado BOOLEAN NOT NULL, imagenes JSON DEFAULT NULL, idioma VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE contacto (id INT NOT NULL, tipo VARCHAR(255) NOT NULL, informacion JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE subseccion (id INT NOT NULL, seccion_id INT NOT NULL, plantilla_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, orden INT NOT NULL, banner JSON DEFAULT NULL, desc_titulo VARCHAR(255) DEFAULT NULL, grupo INT NOT NULL, contenido JSON DEFAULT NULL, publicada BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_abdfbd06a08f3969 ON subseccion (plantilla_id)');
        $this->addSql('CREATE INDEX idx_abdfbd067a5a413a ON subseccion (seccion_id)');
        $this->addSql('CREATE TABLE caracteristica (id INT NOT NULL, titulo VARCHAR(255) NOT NULL, cuerpo TEXT NOT NULL, imagenes JSON DEFAULT NULL, orden INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE estadistica (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, valor VARCHAR(255) NOT NULL, grupo INT NOT NULL, habilitada BOOLEAN NOT NULL, idioma VARCHAR(2) NOT NULL, home BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE informacion_subseccion ADD CONSTRAINT fk_77b4435a25cba699 FOREIGN KEY (subseccion_id) REFERENCES subseccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logistica ADD CONSTRAINT fk_5d33cc21b9087426 FOREIGN KEY (caracteristicas_id) REFERENCES caracteristica (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subseccion ADD CONSTRAINT fk_abdfbd067a5a413a FOREIGN KEY (seccion_id) REFERENCES seccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subseccion ADD CONSTRAINT fk_abdfbd06a08f3969 FOREIGN KEY (plantilla_id) REFERENCES plantilla (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE configuracion ADD tipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion ADD fecha_publicada TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE configuracion ADD publicada BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE configuracion ADD idioma VARCHAR(2) DEFAULT NULL');
        $this->addSql('ALTER TABLE configuracion RENAME COLUMN tipo TO valor');
        $this->addSql('ALTER TABLE configuracion ADD CONSTRAINT fk_682ccaf1a9276e6c FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_682ccaf1a9276e6c ON configuracion (tipo_id)');
    }
}
