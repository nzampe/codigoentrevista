<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240223184124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE home_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE version_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE subtipo_id_seq CASCADE');
        $this->addSql('ALTER TABLE subtipo DROP CONSTRAINT fk_a9986fcca9276e6c');
        $this->addSql('DROP TABLE home');
        $this->addSql('DROP TABLE version');
        $this->addSql('DROP TABLE subtipo');
        $this->addSql('ALTER TABLE configuracion_subtipo ADD tipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion_subtipo ADD CONSTRAINT FK_5F50A361A9276E6C FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5F50A361A9276E6C ON configuracion_subtipo (tipo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE home_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE version_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE subtipo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE home (id INT NOT NULL, contenido JSON NOT NULL, version INT NOT NULL, idioma VARCHAR(2) NOT NULL, publicada BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE version (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE subtipo (id INT NOT NULL, tipo_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_a9986fcca9276e6c ON subtipo (tipo_id)');
        $this->addSql('ALTER TABLE subtipo ADD CONSTRAINT fk_a9986fcca9276e6c FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE configuracion_subtipo DROP CONSTRAINT FK_5F50A361A9276E6C');
        $this->addSql('DROP INDEX IDX_5F50A361A9276E6C');
        $this->addSql('ALTER TABLE configuracion_subtipo DROP tipo_id');
    }
}
