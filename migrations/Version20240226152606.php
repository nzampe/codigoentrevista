<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240226152606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE configuracion ADD tipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion RENAME COLUMN tipo TO valor');
        $this->addSql('ALTER TABLE configuracion ADD CONSTRAINT FK_682CCAF1A9276E6C FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_682CCAF1A9276E6C ON configuracion (tipo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE configuracion DROP CONSTRAINT FK_682CCAF1A9276E6C');
        $this->addSql('DROP INDEX IDX_682CCAF1A9276E6C');
        $this->addSql('ALTER TABLE configuracion DROP tipo_id');
        $this->addSql('ALTER TABLE configuracion RENAME COLUMN valor TO tipo');
    }
}
