<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240304141758 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE configuracion DROP CONSTRAINT fk_682ccaf1e245c6a3');
        $this->addSql('DROP SEQUENCE configuracion_subtipo_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE configuacion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE configuacion (id INT NOT NULL, valor JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE configuracion_subtipo DROP CONSTRAINT fk_5f50a361a9276e6c');
        $this->addSql('DROP TABLE configuracion_subtipo');
        $this->addSql('DROP INDEX idx_682ccaf1e245c6a3');
        $this->addSql('ALTER TABLE configuracion DROP subtipo_id');
        $this->addSql('ALTER TABLE configuracion DROP valor');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE configuacion_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE configuracion_subtipo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE configuracion_subtipo (id INT NOT NULL, tipo_id INT NOT NULL, codigo VARCHAR(75) NOT NULL, descripcion VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_5f50a361a9276e6c ON configuracion_subtipo (tipo_id)');
        $this->addSql('ALTER TABLE configuracion_subtipo ADD CONSTRAINT fk_5f50a361a9276e6c FOREIGN KEY (tipo_id) REFERENCES configuracion_tipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE configuacion');
        $this->addSql('ALTER TABLE configuracion ADD subtipo_id INT NOT NULL');
        $this->addSql('ALTER TABLE configuracion ADD valor VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE configuracion ADD CONSTRAINT fk_682ccaf1e245c6a3 FOREIGN KEY (subtipo_id) REFERENCES configuracion_subtipo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_682ccaf1e245c6a3 ON configuracion (subtipo_id)');
    }
}
