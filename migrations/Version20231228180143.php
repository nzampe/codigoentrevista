<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231228180143 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE contacto_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE idioma_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE informacion_subseccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE noticia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE producto_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE seccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE subseccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE usuario_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE contacto (id INT NOT NULL, tipo VARCHAR(255) NOT NULL, informacion JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE idioma (id INT NOT NULL, descripcion VARCHAR(255) NOT NULL, codigo VARCHAR(30) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE informacion_subseccion (id INT NOT NULL, subseccion_id INT NOT NULL, contenido TEXT NOT NULL, imagenes JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_77B4435A25CBA699 ON informacion_subseccion (subseccion_id)');
        $this->addSql('CREATE TABLE noticia (id INT NOT NULL, titulo VARCHAR(255) NOT NULL, subtitulo VARCHAR(255) DEFAULT NULL, cuerpo TEXT NOT NULL, fecha TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, imagenes JSON DEFAULT NULL, publicada BOOLEAN NOT NULL, grupo INT NOT NULL, idioma VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE producto (id INT NOT NULL, titulo VARCHAR(255) NOT NULL, desc_titulo VARCHAR(255) NOT NULL, subtitulo VARCHAR(255) DEFAULT NULL, cuerpo TEXT NOT NULL, grupo INT NOT NULL, publicado BOOLEAN NOT NULL, imagenes JSON DEFAULT NULL, idioma VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE seccion (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, tipo_seccion VARCHAR(255) NOT NULL, orden INT NOT NULL, banner JSON DEFAULT NULL, desc_titulo VARCHAR(255) DEFAULT NULL, grupo INT NOT NULL, idioma VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE subseccion (id INT NOT NULL, seccion_id INT NOT NULL, nombre VARCHAR(255) NOT NULL, orden INT NOT NULL, banner JSON DEFAULT NULL, desc_titulo VARCHAR(255) DEFAULT NULL, grupo INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_ABDFBD067A5A413A ON subseccion (seccion_id)');
        $this->addSql('CREATE TABLE usuario (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, f_desde TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, f_hasta TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, f_carga TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2265B05DF85E0677 ON usuario (username)');
        $this->addSql('ALTER TABLE informacion_subseccion ADD CONSTRAINT FK_77B4435A25CBA699 FOREIGN KEY (subseccion_id) REFERENCES subseccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE subseccion ADD CONSTRAINT FK_ABDFBD067A5A413A FOREIGN KEY (seccion_id) REFERENCES seccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE contacto_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE idioma_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE informacion_subseccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE noticia_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE producto_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE seccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE subseccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE usuario_id_seq CASCADE');
        $this->addSql('ALTER TABLE informacion_subseccion DROP CONSTRAINT FK_77B4435A25CBA699');
        $this->addSql('ALTER TABLE subseccion DROP CONSTRAINT FK_ABDFBD067A5A413A');
        $this->addSql('DROP TABLE contacto');
        $this->addSql('DROP TABLE idioma');
        $this->addSql('DROP TABLE informacion_subseccion');
        $this->addSql('DROP TABLE noticia');
        $this->addSql('DROP TABLE producto');
        $this->addSql('DROP TABLE seccion');
        $this->addSql('DROP TABLE subseccion');
        $this->addSql('DROP TABLE usuario');
    }
}
